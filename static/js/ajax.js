
$(document).ready(function () {
    $('#login_in').submit(function (even) {
        //отправляем запрос на сервер
        even.preventDefault();
        $.ajax({
            type:'POST',
            url:'/auth/login/',
            data:{
                username : $('#id_username').val(),
                password : $('#id_password').val(),
                csrfmiddlewaretoken: $("input[name = csrfmiddlewaretoken]").val()
            },

            success:function (data) {
                if (data == 'no'){
                    $('#message_error').html('Не удалось, проверьте еще раз логин и пароль')
                }
                else {
                   window.location.reload();
                }

            }
        });
    });
     $('#new_user_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:'/auth/registration/',
            data:{
                username:$('#name').val(),
                password:$('#password').val(),
                email:$('#email').val(),
                csrfmiddlewaretoken: $("input[name = csrfmiddlewaretoken]").val(),
            },
            success:function (data) {
                if (data=='done'){
                     $('#message_error2').html('Успешная регистрация')
                }
                if (data=='fieldIsNone'){
                     $('#message_error2').html('Не все поля введены корректно')
                }
                  if (data=='sameUser'){
                     $('#message_error2').html('Такое имя уже используется')
                }
                  if (data=='sameEmail'){
                     $('#message_error2').html('Эта почта уже используется')
                }

            }

        });

    });
});

