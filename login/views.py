# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, render_to_response,redirect,HttpResponse
from django.contrib import auth
from django.contrib.auth.models import User
from django.template.context_processors import csrf


# Create your views here.

def login(request):
    args={}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username','')
        password = request.POST.get('password','')
        user = auth.authenticate(username=username, password = password)
        if user is not None:
            auth.login(request,user)
            return render_to_response('SentMessage/FormSent.html', args)
        else:
            return HttpResponse('no',content_type='text/html')
    if request.user.is_authenticated():
        args['username'] = auth.get_user(request).username
        return render_to_response('SentMessage/FormSent.html', args)
    else:
        return render_to_response('AuthUser/FormAuth.html', args)
def registration(request):
    args={}
    args.update(csrf(request))
    if request.POST:
        print ('post')
        userName = request.POST.get('username',None)
        print (userName)
        userEmail = request.POST.get('email',None)
        print (userEmail)
        userPassword = request.POST.get('password',None)
        print (userPassword)
        if User.objects.filter(username=userName).exists():
            return HttpResponse('sameUser', content_type='text/html')
        else:
            if User.objects.filter(email=userEmail).exists():
                return HttpResponse('sameEmail', content_type='text/html')
            else:
                if userName and userEmail and userPassword is not None:
                    user = User.objects.create_user(userName, email=userEmail, password=userPassword)
                    user.save()
                    return HttpResponse('done',content_type='text/html')
                else:
                    return HttpResponse('fieldIsNone',content_type='text/html')


    return redirect('/')