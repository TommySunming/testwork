# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class StatusSendMessage(models.Model):
    user = models.CharField(max_length=30)
    subject = models.CharField(max_length=50)
    status = models.CharField(max_length=15)

    class Meta:
        verbose_name = 'Статус сообщения'
        verbose_name_plural = 'Статус сообщений'