# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from Form.models import *
from django.contrib import admin

# Register your models here.
class StatusAdmin(admin.ModelAdmin):
    #list_display = [field.name for field in StatusSendMessage._meta.fields]
    list_display = ['user','subject','status']

    class Meta:
        model = StatusSendMessage

admin.site.register(StatusSendMessage, StatusAdmin)