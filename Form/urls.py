from django.conf.urls import url, include
from Form import views
urlpatterns = [
    url(r'^auth/', include('login.urls')),
    url(r'^logout/', views.Logout, name='logout'),
    url(r'^sent/',views.SentMessage, name='sent'),
    url(r'^$', views.FormSentMessage, name='FormSentMessage'),


]