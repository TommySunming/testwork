# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response, redirect
from Form.models import StatusSendMessage
from django.core.mail import send_mail
from django.template.context_processors import csrf
from django.contrib import auth


def FormSentMessage(request):
    args={}
    args.update(csrf(request))
    if request.user.is_authenticated():
        args['username'] = auth.get_user(request).username
        return render_to_response('SentMessage/FormSent.html',args)
    else:
        return render_to_response('AuthUser/FormAuth.html',args)

def Logout(request):
    auth.logout(request)
    args={}
    args.update(csrf(request))
    return redirect('/')
def SentMessage(request):
    args={}
    args.update(csrf(request))
    if request.POST:
        subject = request.POST.get('subject',None)
        message = request.POST.get('message',None)
        fromemail = request.POST.get('email',None)
        if subject and message and fromemail is not None:
            try:
                send_mail(subject,message,fromemail,
                  ['timon.sunming@yandex.ru'], fail_silently=False)
                args['status']='Сообщение отправленно'
                newstatus = StatusSendMessage.objects.create(user=auth.get_user(request).username, subject=subject, status='Отправлено')
                newstatus.save()
                return render_to_response('SentMessage/FormSent.html', args)
            except :
                args['status']='Произошла ошибка, сообщение не отправленно'
                newstatus = StatusSendMessage.objects.create(user=auth.get_user(request).username, subject=subject, status='Не отправлено')
                newstatus.save()
                return render_to_response('SentMessage/FormSent.html', args)
        else:
            args['status'] = 'Введите все поля'
            print ('заполните поля')
            return render_to_response('SentMessage/FormSent.html', args)
    return redirect('/')